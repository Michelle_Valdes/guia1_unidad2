import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk


class ventana_principal():

    def __init__(self):
        # Se añade la ventana
        self.builder = Gtk.Builder()
        self.builder.add_from_file("UI.glade")

        # Se llama la ventana y se muestra 
        self.ventana = self.builder.get_object("ventana_principal")
        self.ventana.set_default_size(800, 600)
        self.ventana.connect("destroy", Gtk.main_quit)
        self.ventana.show_all()

        # Crear botones
        self.boton_aceptar = self.builder.get_object("boton_aceptar")
        self.boton_deshacer = self.builder.get_object("boton_deshacer")

        # Crear cuadros de entrada de texto
        self.texto1= self.builder.get_object("texto_1")
        self.texto2= self.builder.get_object("texto_2")

        # Conectar botones
        self.boton_aceptar.connect("clicked", self.aceptar)
        self.boton_deshacer.connect("clicked", self.borrar)

    # Función para sumar los textos
    def sumar(self, btn=None):

        self.entrada1 = self.texto1.get_text()
        self.entrada2 = self.texto2.get_text()

        # Se intenta convertir los textos a numero
        try:
            valor1 = int(self.entrada1)
            valor2 = int(self.entrada2)
 
        # Si es posible, se toma el largo de las cadenas
        except:
            valor1 = len(self.entrada1)
            valor2 = len(self.entrada2)


        self.suma = valor1 + valor2


    #Crear ventanita de resultados
    def aceptar(self, btn=None):
        self.sumar()
        self.ventana_resultados = resultados(self.entrada1, self.entrada2,  self.suma)

    def borrar(self, btn=None):
        self.advertencia = advertencia()
        respuesta = self.advertencia.ventana.run()
        
        if respuesta == Gtk.ResponseType.OK:
            self.texto1.set_text("")
            self.texto2.set_text("")



class resultados():
    def __init__(self, texto1, texto2, resultado):

        self.builder = Gtk.Builder()
        self.builder.add_from_file("UI.glade")

        self.ventana = self.builder.get_object("ventana_resultados")
        # Se escribe lo que debe aparecer en la ventana de resultados
        self.ventana.set_markup("usted ingreso los textos: "
                                 f"{texto1} y {texto2}. \n"
                                 f"Estos suman {resultado}")
        
        self.ventana.show_all()

        #boton aceptar
        self.boton_aceptar = self.builder.get_object("resultados_aceptar")
        self.boton_aceptar.connect("clicked", self.cerrar_ventana)

    def cerrar_ventana(self, btn=None):
         self.ventana.destroy()



class advertencia():

    def __init__(self):

        self.builder = Gtk.Builder()
        self.builder.add_from_file("UI.glade")

        self.ventana = self.builder.get_object("ventana_advertencia")
        self.ventana.set_markup("¿¡QUIERES BORRAR LOS TEXTOS!?")
        self.ventana.show_all()

        self.boton_aceptar = self.builder.get_object("advertencia_aceptar")
        self.boton_cancelar = self.builder.get_object("advertencia_cancelar")

        self.boton_aceptar.connect("clicked", self.aceptar)
        self.boton_cancelar.connect("clicked", self.cancelar)

    def cancelar(self, btn=None):
        self.ventana.destroy()

    def aceptar(self, btn=None):
        self.ventana.destroy()



if __name__ == "__main__":

    ventana_principal()
    Gtk.main()
